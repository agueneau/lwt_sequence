type 'a node = {
  mutable prev : 'a node;
  mutable next : 'a node;
  mutable data : 'a;
}

let create (data: 'a): 'a node =
  let rec node = { prev = node; next = node; data } in
  node

let get (node: 'a node): 'a =
  node.data

let set (node: 'a node) (data: 'a): unit =
  node.data <- data

let next (node: 'a node): 'a node =
  node.next

let prev (node: 'a node): 'a node =
  node.prev

let splice (node1: 'a node) (node2: 'a node): unit =
  let next = node1.next in
  let prev = node2.prev in
  node1.next <- node2;
  node2.prev <- node1;
  next.prev <- prev;
  prev.next <- next

(* Remove [node] from the list it is part of, and make it a singleton list. *)
let remove (node: 'a node): unit =
  splice node node

let is_singleton (node: 'a node): bool =
  node.next == node

let length (node: 'a node): int =
  let rec loop curr len =
    if curr == node then
      len
    else
      loop curr.next (len + 1)
  in
  loop node.next 1

let add_l (data: 'a) (node: 'a node): 'a node =
  let new_node = { prev = node.prev; next = node; data } in
  node.prev.next <- new_node;
  node.prev <- new_node;
  new_node

let add_r (data: 'a) (node: 'a node): 'a node =
  let new_node = { prev = node; next = node.next; data } in
  node.next.prev <- new_node;
  node.next <- new_node;
  new_node

let take_opt_l (node: 'a node): 'a option =
  if is_singleton node then
    None
  else begin
    let prev = node.prev in
    remove prev;
    Some (get prev)
  end

let take_opt_r (node: 'a node): 'a option =
  if is_singleton node then
    None
  else begin
    let next = node.next in
    remove next;
    Some (get next)
  end

let iter_node (f: 'a node -> unit) (node: 'a node): unit =
  let () = f node in
  let rec loop n =
    if n != node then
      let next = n.next in
      let () = f n in
      loop next
  in
  loop node.next

let iter_node_rev (f: 'a node -> unit) (node: 'a node): unit =
  let () = f node in
  let rec loop n =
    if n != node then
      let prev = n.prev in
      let () = f n in
      loop prev
  in
  loop node.prev
