type 'a node

val create : 'a -> 'a node

val get : 'a node -> 'a
val set : 'a node -> 'a -> unit

val next : 'a node -> 'a node
val prev : 'a node -> 'a node

val is_singleton : 'a node -> bool
val length : 'a node -> int

val remove : 'a node -> unit

val add_l : 'a -> 'a node -> 'a node
val add_r : 'a -> 'a node -> 'a node

val take_opt_l : 'a node -> 'a option
val take_opt_r : 'a node -> 'a option

val splice : 'a node -> 'a node -> unit

val iter_node : ('a node -> unit) -> 'a node -> unit
val iter_node_rev : ('a node -> unit) -> 'a node -> unit
