(* A [None] node denotes the sentinel *)
type 'a node = 'a option Cycles.node

(* Must be a sentinel node. *)
type 'a t = 'a node

let get (node: 'a node): 'a =
  match Cycles.get node with
  | Some x -> x
  | None -> assert false

let set (node: 'a node) (data: 'a): unit =
  Cycles.set node (Some data)

let next = Cycles.next
let prev = Cycles.prev

let remove (node: 'a node): unit =
  Cycles.remove node

let create (): 'a t =
  Cycles.create None

let is_empty (seq: 'a t): bool =
  Cycles.is_singleton seq

let length (seq: 'a t): int =
  Cycles.length seq - 1

let add_l (data: 'a) (seq: 'a t): 'a node =
  Cycles.add_r (Some data) seq

let add_r (data: 'a) (seq: 'a t): 'a node =
  Cycles.add_l (Some data) seq

let take_opt_l (seq: 'a t): 'a option =
  match Cycles.take_opt_r seq with
  | Some x -> x (* is a [Some _] *)
  | None -> None

let take_opt_r (seq: 'a t): 'a option =
  match Cycles.take_opt_l seq with
  | Some x -> x (* is a [Some _] *)
  | None -> None

let transfer_l (s1: 'a t) (s2: 'a t): unit =
  Cycles.splice s2 s1;
  Cycles.remove s1

let transfer_r (s1: 'a t) (s2: 'a t): unit =
  Cycles.splice s1 s2;
  Cycles.remove s1

let iter_l (f: 'a -> unit) (seq: 'a t): unit =
  Cycles.iter_node (fun node ->
    match Cycles.get node with
    | Some x -> f x
    | None -> ()
  ) seq

let iter_r (f: 'a -> unit) (seq: 'a t): unit =
  Cycles.iter_node_rev (fun node ->
    match Cycles.get node with
    | Some x -> f x
    | None -> ()
  ) seq

let iter_node_l (f: 'a node -> unit) (seq: 'a t): unit =
  Cycles.iter_node (fun node ->
    match Cycles.get node with
    | Some _ -> f node
    | None -> ()
  ) seq

let iter_node_r (f: 'a node -> unit) (seq: 'a t): unit =
  Cycles.iter_node_rev (fun node ->
    match Cycles.get node with
    | Some _ -> f node
    | None -> ()
  ) seq

let fold_l (f: 'a -> 'b -> 'b) (seq: 'a t) (acc: 'b): 'b =
  failwith "todo"

let fold_r (f: 'a -> 'b -> 'b) (seq: 'a t) (acc: 'b): 'b =
  failwith "todo"

let find_node_opt_l (f: 'a -> bool) (seq: 'a t): 'a node option =
  failwith "todo"

let find_node_opt_r (f: 'a -> bool) (seq: 'a t): 'a node option =
  failwith "todo"
