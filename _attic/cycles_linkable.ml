type ('record, 'data) field = {
  get: 'record -> 'data;
  set: 'record -> 'data -> unit;
}

module Linkable = struct
  type 'node t = {
    prev : ('node, 'node) field;
    next : ('node, 'node) field;
  }

  let next (l: 'node t) (node: 'node): 'node =
    l.next.get node

  let prev (l: 'node t) (node: 'node): 'node =
    l.prev.get node

  let is_singleton (l: 'node t) (node: 'node): bool =
    (l.next.get node) == node

  let length (l: 'node t) (node: 'node): int =
    let rec loop curr len =
      if curr == node then
        len
      else
        loop (l.next.get curr) (len + 1)
    in
    loop (l.next.get node) 1

  let splice (l: 'node t) (node1: 'node) (node2: 'node): unit =
    let next = l.next.get node1 in
    let prev = l.prev.get node2 in
    l.next.set node1 node2;
    l.prev.set node2 node1;
    l.prev.set next prev;
    l.next.set prev next

  (* Remove [node] from the list it is part of, and make it a singleton list. *)
  let remove (l: 'node t) (node: 'node): unit =
    splice l node node

  (*
  let hide (l: 'node t) (node: 'node): unit =
    let next = l.next.get node in
    if next == node then ()
    else begin
      let prev = l.prev.get node in
      l.next.set prev next;
      l.prev.set next prev
    end

  let show (l: 'node t) (node: 'node): unit =
    let next = l.next.get node in
    if next == node then ()
    else begin
      let prev = l.prev.get node in
      l.next.set prev node;
      l.prev.set next node
    end
  *)

  let iter (l: 'node t) (f: 'node -> unit) (node: 'node): unit =
    let () = f node in
    let rec loop n =
      if n != node then
        let () = f n in
        loop (l.next.get n)
    in
    loop (l.next.get node)

  let iter_rev (l: 'node t) (f: 'node -> unit) (node: 'node): unit =
    let () = f node in
    let rec loop n =
      if n != node then
        let () = f n in
        loop (l.prev.get n)
    in
    loop (l.prev.get node)
end

type 'a node = {
  mutable prev : 'a node;
  mutable next : 'a node;
  mutable data : 'a;
}

let linkable : 'a node Linkable.t = {
  prev = {
    get = (fun n -> n.prev);
    set = (fun n x -> n.prev <- x);
  };
  next = {
    get = (fun n -> n.next);
    set = (fun n x -> n.next <- x);
  };
}

let create (data: 'a): 'a node =
  let rec node = { prev = node; next = node; data } in
  node

let get (node: 'a node): 'a =
  node.data

let set (node: 'a node) (data: 'a): unit =
  node.data <- data

let next (node: 'a node): 'a node =
  node.next

let prev (node: 'a node): 'a node =
  node.prev

let remove (node: 'a node): unit =
  Linkable.remove linkable node

let is_singleton (node: 'a node): bool =
  Linkable.is_singleton linkable node

let length (node: 'a node): int =
  Linkable.length linkable node

let add_l (data: 'a) (node: 'a node): 'a node =
  let new_node = { prev = node.prev; next = node; data } in
  node.prev.next <- new_node;
  node.prev <- new_node;
  new_node

let add_r (data: 'a) (node: 'a node): 'a node =
  let new_node = { prev = node; next = node.next; data } in
  node.next.prev <- new_node;
  node.next <- new_node;
  new_node

let take_opt_l (node: 'a node): 'a option =
  if is_singleton node then
    None
  else begin
    let prev = node.prev in
    remove prev;
    Some (get prev)
  end

let take_opt_r (node: 'a node): 'a option =
  if is_singleton node then
    None
  else begin
    let next = node.next in
    remove next;
    Some (get next)
  end

let splice (node1: 'a node) (node2: 'a node): unit =
  Linkable.splice linkable node1 node2

(*
let hide (node: 'a node): unit =
  Linkable.hide linkable node

let show (node: 'a node): unit =
  Linkable.show linkable node
*)

let iter (f: 'a node -> unit) (node: 'a node): unit =
  Linkable.iter linkable f node

let iter_rev (f: 'a node -> unit) (node: 'a node): unit =
  Linkable.iter_rev linkable f node
