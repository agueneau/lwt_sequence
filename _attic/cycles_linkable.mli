type 'a node

val create : 'a -> 'a node

val get : 'a node -> 'a
val set : 'a node -> 'a -> unit

val next : 'a node -> 'a node
val prev : 'a node -> 'a node

val is_singleton : 'a node -> bool
val length : 'a node -> int

val remove : 'a node -> unit

val add_l : 'a -> 'a node -> 'a node
val add_r : 'a -> 'a node -> 'a node

val take_opt_l : 'a node -> 'a option
val take_opt_r : 'a node -> 'a option

val splice : 'a node -> 'a node -> unit

(*
val hide : 'a node -> unit
val show : 'a node -> unit
*)

val iter : ('a node -> unit) -> 'a node -> unit
val iter_rev : ('a node -> unit) -> 'a node -> unit

(******************************************************************************)

type ('record, 'data) field = {
  get: 'record -> 'data;
  set: 'record -> 'data -> unit;
}

module Linkable : sig
  type 'node t = {
    prev : ('node, 'node) field;
    next : ('node, 'node) field;
  }

  val next : 'node t -> 'node -> 'node
  val prev : 'node t -> 'node -> 'node

  val is_singleton : 'node t -> 'node -> bool
  val length : 'node t -> 'node -> int

  val remove : 'node t -> 'node -> unit
  val splice : 'node t -> 'node -> 'node -> unit

(*
  val hide : 'node t -> 'node -> unit
  val show : 'node t -> 'node -> unit
*)

  val iter : 'node t -> ('node -> unit) -> 'node -> unit
  val iter_rev : 'node t -> ('node -> unit) -> 'node -> unit
end
