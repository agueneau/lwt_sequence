(*
**  LibPermutation.v: Abstract permutations.
*)

Set Implicit Arguments.

Require Import Stdlib.
Require Import TLC.LibInt.
Require Import TLC.LibSet.
Require Import TLC.LibTactics.
Require Import TLC.LibCore.
Require Import Extensions.

Open Scope set_scope.

Section Permutation.
Context (A: Type).

Record Permutation (D: set A) (ϕ: A -> A) (ψ: A -> A) := {
  Pinvϕ: forall k, k ∈ D -> ψ (ϕ k) = k;
  Pinvψ: forall k, k ∈ D -> ϕ (ψ k) = k;
  Pimgϕ: forall k, k ∈ D -> ϕ k ∈ D;
  Pimgψ: forall k, k ∈ D -> ψ k ∈ D;
}.

(* Don't provide [k] since we already provide [k ∈ D]. *)
Arguments Pinvϕ [D] [ϕ] [ψ] p [k].
Arguments Pinvψ [D] [ϕ] [ψ] p [k].
Arguments Pimgϕ [D] [ϕ] [ψ] p [k].
Arguments Pimgψ [D] [ϕ] [ψ] p [k].

(** Injectivity of permutations **)

Lemma Pinjϕ: forall D ϕ ψ (P: Permutation D ϕ ψ) i j,
  i ∈ D -> j ∈ D -> ϕ i = ϕ j -> i = j.
Proof.
  introv P Di Dj Heq.
  rewrites <- (>> Pinvϕ P Di).
  rewrites <- (>> Pinvϕ P Dj).
  fequal~.
Qed.

Lemma Pinjψ: forall D ϕ ψ (P: Permutation D ϕ ψ) i j,
  i ∈ D -> j ∈ D -> ψ i = ψ j -> i = j.
Proof.
  introv P Di Dj Heq.
  rewrites <- (>> Pinvψ P Di).
  rewrites <- (>> Pinvψ P Dj).
  fequal~.
Qed.

Arguments Pinjϕ [D] [ϕ] [ψ] P [i] [j].
Arguments Pinjψ [D] [ϕ] [ψ] P [i] [j].



(*
let rec nodes f y x = if y = x then [] else y :: nodes f (f y) x
*)

Require Import FunInd Recdef.

Definition mes D : set A -> nat :=
  fun X => card (D \- X).

Lemma set_cons_incl : forall (x:A) s s',
  s \c s' ->
  x \in s' ->
  (s \++ x) \c s'.
Admitted.

Lemma card_diff_incl : forall A (s1 s2: set A),
  finite s1 ->
  s2 \c s1 ->
  card (s1 \- s2) = (card s1 - card s2)%nat.
Admitted.

Function nodes
  D P N
  (seen: set A) (y x: A)
  (HP: Permutation D P N)
  (HD: finite D)
  (Hseen: seen \c D)
  (Hx: x \in seen)
  (Hy: y \in D)
  { measure (mes D) seen }
: list A
:=
  If y = x then nil
  else y :: (@nodes D P N (seen \u \{y}) (N y) x HP HD
                    (set_cons_incl Hseen Hy)
                    (in_union_l Hx)
                    (Pimgψ HP Hy)).
Proof.
  introv HP HD Hseen Hx Hy. intros Hyx _.
  unfold mes.
  rewrites~ card_diff_incl; [|applys~ set_cons_incl].
  rewrites~ card_diff_incl.
  asserts: (card D >= card seen)%nat.  admit.
  asserts: (card D >= card (seen \++ y))%nat.  admit.
  enough (card (seen \++ y) > card seen) by math.
  rewrites~ card_disjoint_union_single.
  now math. now applys~ finite_incl D.
Abort.

Require Import TLC.LibFix.

(*
let rec nodes f y x = if y = x then [] else y :: nodes f (f y) x
*)

Definition nodes_rec nodes (F: A -> A) (y x: A) :=
  If y = x then nil
  else y :: nodes F (F y) x.

Definition nodes := FixFun3 nodes_rec.

Lemma fix_nodes : forall D P N y x seen,
  Permutation D P N ->
  (forall z, z ∈ seen ->
        z = x
     \/ (exists u, z = N u /\ u ∈ seen)) ->
  x ∈ seen ->
  y ∈ D ->
  finite D ->
  seen ⊆ D ->
  nodes N x y = nodes_rec nodes N x y.
Proof.
  introv HP Hseen Hx HyD HD HseenD.
Admitted.

Lemma fix_nodes_same : forall D P N x,
  Permutation D P N ->
  finite D ->
  x ∈ D ->
  nodes N x x = nodes_rec nodes N x x.
Proof.
  introv HP HD Hx. applys~ (>>fix_nodes D P N x x (\{x}) HP).
  { rew_set. auto. }
  { rew_set. intro. rew_set. intros ->. auto. }
Qed.

End Permutation.
