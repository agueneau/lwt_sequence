Set Implicit Arguments.
Require Import Equations.Equations.
Require Import CFML.CFLib.
Require Import CFML.Stdlib.Pervasives_proof.
Require Cycles_ml.

Require Import Extensions.
Require Import LibPermutation.
From TLC Require Import LibSet LibMap LibFun LibRelation.

Ltac auto_tilde ::= eauto with maths typeclass_instances.

Section Cycles.
Context {A: Type}.
Context {IA: Inhab A}.

Record node := mknode {
  node_prev: loc;
  node_next: loc;
  node_data: A;
}.

Instance Inhab_node : Inhab node.
Proof.
  constructor. eexists; [| apply I].
  apply mknode; apply arbitrary.
Qed.

Definition Node : htype node loc := fun n r =>
  r ~> `{ Cycles_ml.prev' := node_prev n;
          Cycles_ml.next' := node_next n;
          Cycles_ml.data' := node_data n }.

Lemma Node_unfold : forall r n,
  Node n r ==>
  r ~> `{ Cycles_ml.prev' := node_prev n;
          Cycles_ml.next' := node_next n;
          Cycles_ml.data' := node_data n }.
Proof. intros. xunfold Node. hsimpl. Qed.

Lemma Node_fold : forall r p n dat,
  r ~> `{ Cycles_ml.prev' := p;
          Cycles_ml.next' := n;
          Cycles_ml.data' := dat } ==>
  Node (mknode p n dat) r.
Proof. intros. xunfold Node. hsimpl. Qed.

Hint Extern 1 (RegisterOpen (Node _)) => Provide Node_unfold.
Hint Extern 1 (RegisterClose (record_repr _)) => Provide Node_fold.

Implicit Types D : set loc.
Implicit Types X: map loc A.
Implicit Types P: loc -> loc.
Implicit Types N: loc -> loc.

Definition nodes D X P N : map loc node := fun n =>
  If n ∈ D then Some (mknode (P n) (N n) X[n])
  else None.

Section S.
Transparent LibMap.dom_inst LibMap.read_inst.

Lemma dom_nodes_eq : forall D X P N,
  dom (nodes D X P N) = D.
Proof.
  intros. simpl. unfold dom_impl.
  rew_set. intro n. split; intro Hn; rew_set in *.
  { unfold nodes in Hn. case_if~. }
  { unfold nodes; case_if~. congruence. }
Qed.

Lemma nodes_read_eq : forall n D X P N,
  n ∈ D ->
  (nodes D X P N)[n] = mknode (P n) (N n) X[n].
Proof.
  intros. simpl. unfold nodes, read_impl. case_if~.
Qed.

End S.

Lemma Group_rem_fold : forall A `{Inhab A} a x (M: map a A) (G: htype A a),
  x \indom M ->
  Group G (M \-- x) \* x ~> G M[x] ==> Group G M.
Proof.
  introv Hx. rewrites~ (>> Group_rem x M).
Qed.

Lemma indom_nodes : forall n D X P N,
  n ∈ D ->
  n \indom (nodes D X P N).
Proof. intros. rewrites~ dom_nodes_eq. Qed.

Hint Resolve indom_nodes.

Lemma Nodes_get_next_spec : forall n D X P N,
  n ∈ D ->
  app_keep record_get [n Cycles_ml.next']
    (Group Node (nodes D X P N))
    \[= N n].
Proof.
  introv Hn.
  xchange~ (>>Group_rem n).
  xopen n. xapp. xpull;=> E. xclose n.
  xchange~ Group_rem_fold. hsimpl~.
  rewrites~ nodes_read_eq in E.
Qed.

Lemma Nodes_set_next_spec : forall n n' D X P N,
  n ∈ D ->
  app record_set [n Cycles_ml.next' n']
    (Group Node (nodes D X P N))
    (# Group Node (nodes D X P N[n:=n'])).
Proof.
  introv Hn. xchange~ (>>Group_rem n).
  xopen n. xapp. intros _. rewrites~ nodes_read_eq; simpl. xclose n.
  assert (nodes D X P N \-- n = nodes D X P N[n:=n'] \-- n) as ->.
  { admit. }
  xchange~ Group_rem_fold.
  rewrite~ nodes_read_eq. simpl. rewrite fupdate_same. hsimpl.
Qed.

Lemma Nodes_get_prev_spec : forall n D X P N,
  n ∈ D ->
  app_keep record_get [n Cycles_ml.prev']
    (Group Node (nodes D X P N))
    \[= P n].
Proof.
  (* FIXME copy paste from above *)
  introv Hn.
  xchange~ (>>Group_rem n).
  xopen n. xapp. xpull;=> E. xclose n.
  xchange~ Group_rem_fold. hsimpl~.
  rewrites~ nodes_read_eq in E.
Qed.

Lemma Nodes_set_prev_spec : forall n n' D X P N,
  n ∈ D ->
  app record_set [n Cycles_ml.prev' n']
    (Group Node (nodes D X P N))
    (# Group Node (nodes D X P[n:=n'] N)).
Proof.
  (* FIXME copy paste from above *)
  introv Hn. xchange~ (>>Group_rem n).
  xopen n. xapp. intros _. rewrites~ nodes_read_eq; simpl. xclose n.
  assert (nodes D X P N \-- n = nodes D X P[n:=n'] N \-- n) as ->.
  { admit. }
  xchange~ Group_rem_fold.
  rewrite~ nodes_read_eq. simpl. rewrite fupdate_same. hsimpl.
Qed.

Ltac xspec_Nodes :=
  match goal with
  | |- app record_get [?r Cycles_ml.prev'] ?H _ =>
    match H with context [ Group Node (nodes _ _ _ _) ] =>
      generalize Nodes_get_prev_spec
    end
  | |- app record_get [?r Cycles_ml.next'] ?H _ =>
    match H with context [ Group Node (nodes _ _ _ _) ] =>
      generalize Nodes_get_next_spec
    end
  | |- app record_set [?r Cycles_ml.prev' _] ?H _ =>
    match H with context [ Group Node (nodes _ _ _ _) ] =>
      generalize Nodes_set_prev_spec
    end
  | |- app record_set [?r Cycles_ml.next' _] ?H _ =>
    match H with context [ Group Node (nodes _ _ _ _) ] =>
      generalize Nodes_set_next_spec
    end
  end.

Ltac xspec_for_record_orig f :=
  match f with
  | record_get => xspec_record_get_compute tt
  | record_set => xspec_record_set_compute tt
  end.

Ltac xspec_for_record f ::=
  first [ xspec_Nodes
        | xspec_for_record_orig f ].

Opaque Extensions.update_inst.

Record Inv D X := {
  data_dom : dom X = D;
  dom_finite : LibSet.finite D;
}.

Definition Cycles D X P N :=
  Group Node (nodes D X P N) \*
  \[ Permutation D N P ] \*
  \[ Inv D X ].

Ltac fupdate_case :=
  rewrite fupdate_eq; case_if~.
Ltac fupdate_case_in H :=
  rewrite fupdate_eq in H; case_if~.
Ltac fupdate_case_any :=
  first [ fupdate_case |
          match goal with H: context [fupdate _ _ _] |- _ => fupdate_case_in H end
        ].

Ltac exploit_inj :=
  match goal with HP : Permutation _ ?N ?P |- _ =>
  match goal with
  | H : N ?x = N ?y |- _ => forwards~ ->: Pinjϕ x y H
  | H : P ?x = P ?y |- _ => forwards~ ->: Pinjψ x y H
  end
  end.

Ltac exploit_inv :=
  match goal with HP : Permutation _ ?N ?P |- _ =>
  match goal with
  | H : context [ P (N ?x) ] |- _ => rewrites~ (>> Pinvϕ x) in H
  | |- context [ P (N ?x) ] => rewrites~ (>> Pinvϕ x)
  | H : context [ N (P ?x) ] |- _ => rewrites~ (>> Pinvψ x) in H
  | |- context [ N (P ?x) ] => rewrites~ (>> Pinvψ x)
  end
  end.

Ltac exploit_perm :=
  repeat first [exploit_inv|exploit_inj].

Ltac crush :=
  repeat fupdate_case_any; substs~; exploit_perm; try congruence.

Hint Resolve Pimgϕ Pimgψ.

Lemma splice_spec : forall n1 n2 D X P N,
  n1 ∈ D ->
  n2 ∈ D ->
  app Cycles_ml.splice [n1 n2]
    PRE (Cycles D X P N)
    POST (# Cycles D X P[n2:=n1][N n1:=P n2] N[n1:=n2][P n2:=N n1]).
Proof.
  introv Hn1 Hn2. xcf.
  xunfold Cycles; xpull;=> HP HI.
  xapps~. xapps~. xapp~. xapp~. xapp~. xapp~.
  hsimpl.

  { constructor; apply HI. }
  Transparent Extensions.update_inst.
  { simpl. constructor; intros; crush. }
  Unshelve. solve_type.
Qed.

Lemma remove_spec : forall n D X P N,
  n ∈ D ->
  app Cycles_ml.remove [n]
    PRE (Cycles D X P N)
    POST (# Cycles D X P[n:=n][N n:=P n] N[n:=n][P n := N n]).
Proof.
  introv Hn. xcf. xapp_spec~ splice_spec.
  Unshelve. solve_type.
Qed.

(* pb: n'interdit pas de prendre les boucles un nombre arbitraire de fois, et
  donc pas d'unicité de la liste l... *)
(* Inductive path (F: loc -> loc) : *)
(*   loc -> loc -> list loc -> Prop *)
(* := *)
(*   | path_edge : forall x y, *)
(*       F x = y -> *)
(*       path F x y nil *)
(*   | path_step : forall x y z l, *)
(*       F x = y -> *)
(*       path F y z l -> *)
(*       path F x z (y :: l). *)


(* Lemma is_singleton_spec : forall n D X P N, *)
(*   n ∈ D -> *)
(*   app Cycles_ml.is_singleton [n] *)
(*     INV (Cycles D X P N) *)
(*     POST (fun (b:bool) => *)
(*       \[ if b then path N n n nil *)
(*          else exists l, l <> nil /\ path N n n l ]). *)