Set Implicit Arguments.

Require Import Stdlib.
Require Import FunctionalExtensionality.

Require Import TLC.LibContainer.
Require Import TLC.LibFun.
Require Import TLC.LibInt.
Require Import TLC.LibListZ.
Require Import TLC.LibMap.
Require Import TLC.LibMonoid.
Require Import TLC.LibSet.
Require Import TLC.LibTactics.
Require Import TLC.LibCore.

(** Sets **)

(* (just for convenience really) *)
Notation "e ∈ X"   := (e \in X) (at level 30).
Notation "e ∉ X"   := (e \notin X) (at level 30).
Notation "X ⊆ Y"   := (X \c Y) (at level 29).
Notation "M \++ y" := (M \u \{ y }) (at level 31).
Notation "\set{ x ∈ E | P }" := (@set_st _ (fun x => x \in E /\ P))
 (at level 0, x ident, P at level 200) : set_scope.

(** Functional update **)

(* Make notation f[x := y] available for functions through fupdate *)
Instance update_inst : forall A B, BagUpdate A B (A -> B).
Proof. constructor. apply (@fupdate A B). Defined.
