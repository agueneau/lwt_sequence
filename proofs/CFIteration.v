Set Implicit Arguments.
Require Import TLC.LibTactics TLC.LibListZ TLC.LibInt.
Require Import CFML.CFHeaps CFML.CFApp CFML.CFPrint CFML.CFTactics.

(* -------------------------------------------------------------------------- *)

(* TEMPORARY future work:

   reformulate using LibSeq instead of separate permitted/complete
   predicates

   merge Cascade_spec.v and CFIteration.v

   give a simplified version of [Fold] where [permitted]
   / [complete] is instantiated with [deterministic]. Are there further
   simplifications?

   give a simplified version of [Fold] where the
   invariant [I] is parameterized with a set of already seen elements,
   instead of a list. Perhaps at the same time instantiate [permitted]
   / [complete] by the subsets of a certain fixed set.

*)

(* -------------------------------------------------------------------------- *)

(* Calling conventions for passing an element [x] to the client function [f]. *)

Definition calling_convention A :=
  func -> A -> ~~unit.

(* The simplest calling convention is to just apply [f] to [x]. *)

Definition cc_normal A : calling_convention A :=
  fun f x =>
    app f [x].

(* A common convention when [x] is a pair is to apply [f] to the two components
   of the pair [x]. This is typically used in the OCaml standard library when
   iterating over maps. *)

Definition cc_unboxed A1 A2 : calling_convention (A1 * A2) :=
  fun f x =>
    app f [(fst x) (snd x)].

(* -------------------------------------------------------------------------- *)

(* Calling conventions for passing an element [x] and an accumulator [accu] to
   the client function [f]. *)

Definition calling_convention_accu A B :=
  func -> A -> B -> ~~B.

Definition cca_normal A B : calling_convention_accu A B :=
  fun f x accu =>
    app f [x accu].

Definition cca_unboxed A1 A2 B : calling_convention_accu (A1 * A2) B :=
  fun f x accu =>
    app f [(fst x) (snd x) accu].

(* -------------------------------------------------------------------------- *)

(* The specification of an [iter] function. *)

Section Iter.

(* The [iter] function. *)

Variable iter : func.

(* The type of the elements that we wish to enumerate. *)

Variable A : Type.

(* The type of the collection, which is the second argument of [iter]. *)

Variable C : Type.

(* The calling convention that is used to pass an element [x] to the user [f]. *)

Variable call : calling_convention A.

(* Which partial sequences of elements are permitted. (That is, which partial
   sequences of elements may be observed by the client.) If [permitted xs] and
   [permitted ys] implies [prefix xs ys \/ prefix ys xs], then the iteration
   order is fully specified -- iteration is deterministic. Otherwise, the order
   is unspecified -- iteration is nondeterministic. *)

(* The predicate [permitted] is always prefix-closed, i.e., [permitted ys]
   and [prefix xs ys] imply [permitted xs]. *)

Variable permitted : list A -> Prop.

(* Which partial sequences of elements are complete. Iteration stops only
   after a complete sequence of elements has been observed by the client.
   Termination nondeterminism is possible in principle, but should seldom
   be useful in practice; most of the time, [prefix xs ys /\ permitted ys
   /\ complete xs /\ complete ys] implies [xs = ys]. *)

Variable complete : list A -> Prop.

(* The user invariant is parameterized by the elements that have been enumerated
   so far. *)

Variable I : list A -> hprop.

(* The state required by [iter], and preserved by [iter]. It typically
   represents the ownership of the collection. In the simple / common
   case, it is invariant. *)

Variable S : C -> hprop.

(* The state shown to the client during iteration. If the client has
   access to the collection during iteration, [S'] could be [S]. If
   the client has no access to the collection during iteration, [S']
   could be empty. *)

Variable S' : C -> hprop.

(* [iter] is a function of two arguments [f] and [c] to unit.

   [f] is itself a function of one argument [x] to unit. It must satisfy the
   following specification. Every time [f] is called, it is passed [I xs], for
   some sequence [xs] of previously seen elements. [f] may assume that the
   sequence [xs & x] is permitted. When it returns, the state must have been
   updated to [I (xs & x)].

   [iter] requires the ownership of the collection, [S c], and preserves it.
   It also requires [I nil], which upon completion is changed to [I xs], for
   some (permitted, complete) sequence [xs] of elements. We assume that
   [complete xs] implies [permitted xs], so we do not write both.
*)

Definition Iter :=
  forall f c,
  (
    forall x xs,
    permitted (xs & x) ->
    call f x
      PRE  (  S' c \* I  xs     )
      POST (# S' c \* I (xs & x))
  ) ->
  app iter [f c]
    PRE  (              S c \* I nil                   )
    POST (# Hexists xs, S c \* I xs \* \[ complete xs ]).

End Iter.

(* -------------------------------------------------------------------------- *)

(* The specification of a [fold] function. Everything is as above, except the
   functions [fold] and [f] and the invariant [I] are parameterized with an
   accumulator of type [B]. *)

Section Fold.

Variable fold : func.
Variables A B C : Type.
Variable call : calling_convention_accu A B.
Variable permitted : list A -> Prop.
Variable complete : list A -> Prop.
Variable I : list A -> B -> hprop.
Variable S : C -> hprop.
Variable S' : C -> hprop.

Definition Fold :=
  forall f c,
  (
    forall x xs accu,
    permitted (xs & x) ->
    call f x accu
      PRE  (            S' c \* I  xs      accu)
      POST (fun accu => S' c \* I (xs & x) accu)
  ) ->
  forall accu,
  app fold [f c accu]
    PRE  (                        S c \* I nil accu                    )
    POST (fun accu => Hexists xs, S c \* I xs  accu \* \[ complete xs ]).

End Fold.

(* -------------------------------------------------------------------------- *)

(* If [P] is a set of sequences, then its derivative [delta x P] is the set
   of the sequences [xs] such that [x :: xs] is a member of the set [P]. *)

(* The derivative can be iterated (from left to right). [delta_star xs P] is
   the derivative of [P] along [xs]. *)

Section Derivative.

Variable A : Type.

Implicit Type P : list A -> Prop.

Definition delta x P :=
  fun xs => P (x :: xs).

Definition delta_star xs P :=
  fold_left delta P xs.

(* A characterization of [delta_star]. This could be a definition. *)

Lemma delta_star_charac:
  forall xs ys zs (P : list A -> Prop),
  zs = xs ++ ys ->
  delta_star xs P ys = P zs.
Proof.
  unfold delta_star.
  induction xs; intros; subst zs; rew_listx in *.
  { eauto. }
  { erewrite IHxs by eauto. eauto. }
Qed.

Lemma delta_star_charac_last:
  forall xs x (P : list A -> Prop),
  delta_star xs P (x :: nil) = P (xs & x).
Proof.
  eauto using delta_star_charac.
Qed.

Lemma delta_star_charac_nil:
  forall xs (P : list A -> Prop),
  delta_star xs P nil = P xs.
Proof.
  intros. eapply delta_star_charac. rew_list. eauto.
Qed.

(* Basic properties of [delta_star]. *)

Lemma delta_star_nil:
  forall (P : list A -> Prop),
  delta_star nil P = P.
Proof.
  reflexivity.
Qed.

Lemma delta_star_app:
  forall xs ys (P : list A -> Prop),
  delta_star (xs ++ ys) P = delta_star ys (delta_star xs P).
Proof.
  unfold delta_star. intros. rewrite fold_left_app. eauto.
Qed.

End Derivative.

(* -------------------------------------------------------------------------- *)

(* The specification of [iter] that was given above is in backward style --
   that is, the invariant is parameterized by the sequence [xs] of elements
   that have been enumerated already. In contrast, one can attempt to give
   a specification in forward style, where the invariant is parameterized by
   the sets of sequences that remain permitted / complete, in light of the
   elements that have been enumerated already. *)

(* It is not clear whether this will be useful, but it should be at least an
   interesting experiment. *)

(* TEMPORARY whenever we quantify over [permitted] and [complete], it seems
   that we should assume or prove [wf (Seqs permitted complete)]? *)

Section ForwardIter.

(* As before. *)

Variable iter : func.
Variable A : Type.
Variable call : calling_convention A.
Variable permitted : list A -> Prop.
Variable complete : list A -> Prop.

(* The invariant is parameterized by the sets of sequences that remain
   permitted / complete. *)

Variable I : (list A -> Prop) -> (list A -> Prop) -> hprop.

(* This is the ``simple'' variant. There is no ``composable'' variant yet. *)

Variable C : Type.
Variables S S' : C -> hprop.

Definition ForwardIter :=
  forall f c,
  (
    forall (permitted complete : list A -> Prop) x,
    permitted (x :: nil) ->
    call f x
      PRE  (  S' c \* I          permitted            complete)
      POST (# S' c \* I (delta x permitted) (delta x complete))
  ) ->
  app iter [f c]
    PRE  (                              S c \* I permitted complete                                      )
    POST (# Hexists permitted complete, S c \* I permitted complete \* \[ complete nil ]).

End ForwardIter.

(* The backward and forward specifications are equivalent. *)

(* Suppose we wish to prove that the forward specification holds, with an invariant [I]
   that is parameterized over the sets of permitted and complete remaining sequences.
   Then it suffices to prove that the backward specification holds, with an invariant
   that says: if we have seen [xs] so far, then the set of permitted (resp. complete)
   remaining sequences is [delta_star xs permitted] (resp. [delta_star xs complete]).
   This means that a sequence of future elements [ys] is permitted (resp. complete) if
   only if [xs ++ ys] was permitted (resp. complete) initially. *)

Lemma backward_to_forward:
  forall iter A call (permitted complete : list A -> Prop) I,
  forall C (S S' : C -> hprop),
  Iter iter call permitted complete
    (fun xs => I (delta_star xs permitted) (delta_star xs complete)) S S' ->
  ForwardIter iter call permitted complete
    I S S'.
Proof.
  introv Hspec.
  intros f c Hf.
  xapply Hspec.
  { intros.
    do 2 rewrite delta_star_app.
    eapply Hf.
    rewrite delta_star_charac_last.
    eauto. }
  { unfold delta_star. simpl. xsimpl. }
  { xpull. intros xs ?.
    xsimpl. rewrite delta_star_charac_nil. eauto. }
Qed.

(* Suppose we wish to prove that the backward specification holds, with an
   invariant [I] that is parameterized over the elements [xs] that have been
   enumerated so far. Then it suffices to prove that the forward specification
   holds, with an invariant that says: if [permitted'] and [complete']
   characterize the sequences of remaining elements, then there must exist
   [xs] such that [xs] has been seen so far and [permitted'] and [complete']
   are the derivatives of the initial [permitted] and [complete] with respect
   to [xs]. *)

Lemma forward_to_backward:
  forall iter A call (permitted complete : list A -> Prop) I,
  forall C (S S' : C -> hprop),
  (forall f x, is_local (call f x)) ->
  ForwardIter iter call permitted complete (fun permitted' complete' =>
    Hexists xs, I xs \* \[
      permitted' = delta_star xs permitted /\
      complete' = delta_star xs complete
    ]
  ) S S' ->
  Iter iter call permitted complete I S S'.
Proof.
  introv Hlocal Hspec.
  intros f c Hf.
  xapply Hspec.
  { intros permitted' complete' x ?.
    xpull; eauto. intros xs [ ? ? ]. subst.
    rewrite delta_star_charac_last in *.
    eapply local_weaken; eauto; xsimpl.
    do 2 rewrite delta_star_app. eauto. }
  { xsimpl. do 2 rewrite delta_star_nil. eauto. }
  { xpull. intros ? ? xs [ ? ? ]. subst.
    rewrite delta_star_charac_nil. intros.
    xsimpl*. }
Qed.
