- François remarks that Lwt_sequence can be built on top of a simpler structure
  of circular (non-empty) lists. (ie the Dllist module of Batteries)
  I call "Cycles" such a module.

  Then the sentinel business can be encoded on top of that, providing a
  different API of (possibly empty) sequences.

- Remark: it may be convenient to expose two different flavors of the API in
  Cycles (modulo some indirections that flambda may be able to optimize away
  (?)).

  + The first API flavor is the usual "OCaml" one: Cycles defines a data
    structure (a linked list of nodes) which stores user data. The node type is
    abstract, and provided by the Cycles module.

    This restricts the way the data is linked: a node cannot belong to several
    linked chains, for example.

  + The second API flavor is in some sense more general, and is AFAIK a common
    pattern in C libraries (eg wayland), where the user defines its own records,
    and linkes them together in various ways by embedding a "linked list" field
    in them. Then, a library provides macro-functions that work on these linked
    structures.

    We can actually replicate this pattern without macros (and with a couple of
    extra indirections), by defining a `('record, 'data) field` type, and
    parameterizing all functions of Cycles with a record that provides a prev
    and next `('node, 'node) field`, and work with any user-provided `'node`
    type..!

    And then the first API can be implemented on top of this one.
    (see `_attic/cycles_linkable.ml{i}`)

- Lwt_sequence actually allows pretty weird stuff. The "active" boolean of a
  node is not here to account for asynchronous removal of nodes (this could be
  expressed as a (non-sentinel) node linked to itself (?)).

  It's to allow removing nodes anywhere (including the current node) in the
  sequence while iterating/folding on it. Example:

```
  .. -- A -- B -- C -- D -- E -- F -- ..
                  ^

  <during a single iteration step> remove C; remove D; remove E

  .. -- A -- B ----------- F -- ..
              \           /
               C -- D -- E
               ^
```

  Now we need to keep the links of the dead nodes (C, D, E) to be able to follow
  them and eventually get back to live nodes -- while being able to distinguish
  between dead nodes and live nodes, to avoid calling the argument function on
  dead nodes that have been removed...

  As one might expect, formalizing this is probably quite hard...

- It's possible to implement this kind of stuff on top of Cycles. One needs to
  add two operations to the Cycles API: `hide` and `show`, of type `'a node -> unit`.

  While `remove` unlinks a node from a cycle and turns it into a self-loop,
  `hide` keeps the links from the node to the structure, allowing to restore it
  later using `show`. Example:

```
  .. -- A -- B -- C -- ..

  hide B

  .. -- A ------- C -- ..
        ^         ^
         \       /
          -- B --

  show B

  .. -- A -- B -- C -- ..
```

  François notes that they (w/ Sébastien) have the same operations on the
  permutations used in dancing links. Not sure if they allow modifying the
  cycled structure though, and there is a restriction that hide/show must be
  called in a "well parenthesized" manner.

  Apart from the unclear spec for `hide` and `show`, having them complicates
  significantly the invariants on the structure and all the other functions.
  Without them, a `'a node` always belong to a cycle (this includes nodes that
  have just been `remove`d which belong to a cycle with only themselves). In
  this setting, all functions of the API make sense in all settings.

  With `hide`/`show`, a `'a node` can also be attached to a cycle and "hidden"
  it. Then, it becomes unsafe to call most functions of the API on hidden
  nodes... (I think?)

- Grepping through opam, it seems the complicated pattern (where nodes that are
  not the current node are deleted during the iteration) is actually never
  used...!

  It happens that the current node gets deleted, though. But this can be
  implemented and specified way more easily.
